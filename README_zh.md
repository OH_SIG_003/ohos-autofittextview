# autofittextview

## 简介
> 自动调整文本大小以完全适合其边界的TextView。

## 效果展示
![sample.gif](sample.gif)

## 下载安装
```shell
ohpm install @ohos/autofittextview
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitcode.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md).

## 使用说明

### 初始化
```
 @State content: string= ''
    @State textSize: number= 20
    @State maxLines: number= 2
    @State width: string= '100%'
    @State model: AutofitTextView.Model= new AutofitTextView.Model()
```

### 设置属性
```
    private aboutToAppear() {
      this.model.setWidth(this.width)
      this.model.setText(this.content)
      this.model.setMaxLines(2)
      this.model.setTextSize(10,TypedValue.COMPLEX_UNIT_PX)
      this.model.setMinTextSize(2)
      this.model.setMaxTextSize(40)
   }
```
### 在build中使用
```
    build() {
      Column() {
        AutofitTextView({ model: this.model });
      }
      .alignItems(HorizontalAlign.Start)
      .padding(10)
      .width('100%')
      .height('100%')
    }
```

## 接口说明
`@State model: AutofitTextView.Model= new AutofitTextView.Model()`
1. 设置控件宽度
   `model.setWidth('100%')`
2. 设置字体大小
   `model.setTextSize(10,TypedValue.COMPLEX_UNIT_PX)`
3. 设置最大字体
   `model.setMaxTextSize(40)`
4. 设置最小字体
   `model.setMinTextSize(2)`
5. 设置最大显示行数
   `model.setMaxLines(2)`
6. 设置文本内容
   `model.setText('test')`
7. 设置背景颜色
   `model.setBackgroundColor(0XD1C9C9)`
8. 设置滚动条大小
   `model.setTextOverflow(32)`
9. 是否开启自适应大小
   `model.isSizeToFit()`
10. 设置开启自适应大小
   `model.setSizeToFit(true)`
11. 获取最大字体
   `model.getMaxTextSize()`
12. 获取最小字体
   `model.getMinTextSize()`
13. 设置正确文本大小的精度值
   `model.setPrecision(1)`
14. 获取正确文本大小的精度值
   `model.getPrecision()`

## 关于混淆
- 代码混淆，请查看[代码混淆简介](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- 如果希望autofittextview库在代码混淆过程中不会被混淆，需要在混淆规则配置文件obfuscation-rules.txt中添加相应的排除规则：
```
-keep
./oh_modules/@ohos/autofittextview
```

## 约束与限制

在下述版本验证通过：

- DDevEco Studio NEXT Developer Beta3: (5.0.3.530), SDK: API12 (5.0.0.35(SP3))
## 目录结构
````
|---- ohos-autofittextview
|     |---- entry  # 示例代码文件夹
|     |---- Autofittextview  # autofittextview库文件夹
                   |---- scr/main/ets/Autofittextview   # 自定义的Text控件
                   |---- scr/main/ets/TypedValue   # 字体大小单位枚举类
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitcode.com/openharmony-sig/ohos-autofittextview/issues) ，当然，也非常欢迎发 [PR](https://gitcode.com/openharmony-sig/ohos-autofittextview/pulls) 共建。

## 开源协议
本项目基于 [Apache License 2.0](https://gitcode.com/openharmony-sig/ohos-autofittextview/blob/master/LICENSE) ，请自由地享受和参与开源。
